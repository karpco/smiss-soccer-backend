var express = require('express');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var moment = require('moment');

var url = process.env.MONGO_CONNECTION ? process.env.MONGO_CONNECTION : 'mongodb://localhost:27017/smiss-soccer';

var app = express();
app.use(bodyParser.json());

app.get("/", function (req, res) {
    res.end("SMISS Soccer API");
});

app.get("/player", function (req, res) {
    MongoClient.connect(url, function (err, db) {
        var cursor = db.collection('players').find();
        cursor.toArray(function (err, players) {
            res.setHeader("Content-Type", "application/json;charset=utf-8");
            res.end(JSON.stringify(players));
            db.close();
        });
    });
});

app.get('/match', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        var cursor = db.collection('matches').find().sort({"date": -1});

        var limit = +(req.query.count || 10);
        if (limit != -1) {
            cursor = cursor.limit(limit);
        }

        cursor.toArray(function (err, matches) {
            if (err) {
                res.status(500);
                res.end(err);
            }

            res.setHeader("Content-Type", "application/json;charset=utf-8");
            res.end(JSON.stringify(matches));
            db.close();
        });
    });
});

app.get('/match/:id', function (req, res) {
    if (!ObjectId.isValid(req.params.id)) {
        res.status(404);
        res.end("Match not found");
        return;
    }

    MongoClient.connect(url, function (err, db) {
        var cursor = db.collection('matches').find({'_id': ObjectId(req.params.id)});

        cursor.next(function (err, match) {
            if (match) {
                res.setHeader("Content-Type", "application/json;charset=utf-8");
                res.end(JSON.stringify(match));
            } else {
                res.status(404);
                res.end('Match not found');
            }
            db.close();
        });
    });
});

app.delete('/match/:id', function (req, res) {
    if (!ObjectId.isValid(req.params.id)) {
        res.status(404);
        res.end("Match not found");
        return;
    }

    MongoClient.connect(url, function (err, db) {
        db.collection('matches').deleteOne({'_id': ObjectId(req.params.id)}, function () {
            res.end();
            db.close();
        });
    });
});

app.put('/match', function (req, res) {
    var match = req.body;

    // convert strings into ObjectId's
    match.teams.forEach(function (team) {
        team.teammates = team.teammates.map(function (teammate) {
            teammate.playerId = ObjectId(teammate.playerId);
            return teammate;
        });
    });

    MongoClient.connect(url, function (err, db) {
        var collection = db.collection('matches');
        collection.insertOne(req.body, function (err, result) {
            db.close();
            res.end();
        });
    });
});

app.get("/stats/elo", function (req, res) {
    MongoClient.connect(url, function (err, db) {
        var eloRates = {};

        db.collection('eloRates').findOne({}, {'sort':['date', 'ascending']}, function (err, rate) {
            if (rate) {
                rate.players.forEach((player) => {
                    eloRates[player.id] = {
                        'attack': player.attack,
                        'defence': player.defence
                    };
                });
            }

            db.collection('players').find().forEach((player) => {
                console.log(player._id);
                if (!eloRates[player._id]) {
                    eloRates[player._id] = {};
                    eloRates[player._id].attack = 1500;
                    eloRates[player._id].defence = 1500;
                    eloRates[player._id].name = player.name;
                    eloRates[player._id].gameCount = 0;
                }
            }, () => {
                console.log('Current Elo rates:');
                console.log(eloRates);

                var pairs = {
                    't1':[null, null],
                    't2':[null, null]
                };
                var matches = db.collection('matches').find({'teamSize': 2}).sort({'date': 1});
                matches.forEach((match) => {
                    console.log(JSON.stringify(match));
                    match.teams.forEach((team) => {
                        if (team.id == 't1') {
                            team.teammates.forEach((teammate) => {
                                assignPair(teammate, pairs.t1, pairs.t2);
                            });
                        } else {
                            team.teammates.forEach((teammate) => {
                                assignPair(teammate, pairs.t2, pairs.t1);
                            });
                        }
                    });
                    console.log(pairs);

                    match.games.forEach((game) => {
                        if (game.t1 == 0 && game.t2 == 0) {
                            return;
                        }

                        recalcElo(eloRates, pairs, game, 't1');
                        recalcElo(eloRates, pairs, game, 't2');

                        console.log(eloRates);

                        pairs = {
                            't1': [pairs.t2[1], pairs.t2[0]],
                            't2': [pairs.t1[1], pairs.t1[0]]
                        };
                    });
                }, function () {
                    let rates = {'attack':[], 'defence':[]};
                    Object.keys(eloRates).forEach((player) => {
                        rates.attack.push({'player': eloRates[player].name, 'rate' : eloRates[player].attack});
                        rates.defence.push({'player': eloRates[player].name, 'rate' : eloRates[player].defence});
                    });
                    rates.attack.sort((p1, p2) => p2.rate - p1.rate);
                    rates.defence.sort((p1, p2) => p2.rate - p1.rate);

                    db.close();
                    res.set({ 'content-type': 'application/json; charset=utf-8' });
                    res.end(JSON.stringify(rates));
                });
            });
        });
    });

    function recalcElo(eloRates, pairs, game, side) {
        console.log('Current rate attack:' + eloRates[pairs[side][0]].attack);
        console.log('Current rate defence:' + eloRates[pairs[side][1]].defence);

        let oppositeSide = side == 't1' ? 't2' : 't1';

        let K = 3;
        let E = 5.0 / (1.0 + Math.pow(10, (eloRates[pairs[side][1]].defence - eloRates[pairs[side][0]].attack) / 400)) -
            (5.0 / (1 + Math.pow(10, (eloRates[pairs[oppositeSide][1]].defence - eloRates[pairs[oppositeSide][0]].attack) / 400))) / 5;
        let newRateAttack = eloRates[pairs[side][0]].attack + calcK(eloRates[pairs[side][0]]) * (game[side] - E);
        let newRateDefence = eloRates[pairs[side][1]].defence + calcK(eloRates[pairs[side][1]]) * (E - game[side] + 2);

        console.log('E=' + E);
        console.log(newRateAttack);
        console.log(newRateDefence);
        eloRates[pairs[side][0]].attack = newRateAttack;
        eloRates[pairs[side][1]].defence = newRateDefence;

        eloRates[pairs[side][0]].gameCount++;
        eloRates[pairs[side][1]].gameCount++;
    }

    function calcK(rate) {
        if (rate.gameCount > 50) {
            return 1.5;
        } else {
            return 4;
        }
    }

    function assignPair(teammate, t1, t2) {
        if (teammate.position == 'атака') {
            t1[0] = teammate.playerId;
        } else {
            t2[1] = teammate.playerId;
        }
    }
});

app.get("/stats/playerStats", function (req, res) {
    MongoClient.connect(url, function (err, db) {
        var stats = [];
        var players = db.collection('players').find().sort({'name': 1});
        players.forEach(function (player) {
            stats.push({
                playerId: player._id,
                totalMatchCount: 0,
                single: {
                    totalGameCount: 0,
                    totalWinGameCount: 0
                },
                double: {
                    totalGameCount: 0,
                    totalWinGameCount: 0,
                    defence: {
                        games: 0,
                        goalsConceded: 0,
                        goalsConcededPerGame: 0
                    },
                    attack: {
                        games: 0,
                        goalsScored: 0,
                        goalsScoredPerGame: 0
                    },
                    simpleScore: 0,
                    past: {
                        defence: {
                            games: 0,
                            goalsConceded: 0,
                            goalsConcededPerGame: 0
                        },
                        attack: {
                            games: 0,
                            goalsScored: 0,
                            goalsScoredPerGame: 0
                        }
                    }
                }
            });
        }, function () {
            async.map(stats, function (playerStats, cb) {
                let query = {'teams.teammates.playerId': ObjectId(playerStats.playerId)};

                if (req.query.season) {
                    query.date = buildSeasonQuery();
                }

                var matches = db.collection('matches')
                    .find(query)
                    .sort({'date': 1});
                matches.count((err, totalMatchCount) => {
                    calcPositionStats(totalMatchCount, matches, playerStats, cb);
                });
            }, function () {
                db.close();

                res.setHeader("Content-Type", "application/json;charset=utf-8");
                res.end(JSON.stringify(stats));
            });
        });
    });
});

function buildSeasonQuery() {
    let from = moment().startOf('month');
    let to = moment().endOf('month');
    return {
        $gte: from.toISOString(), $lte: to.toISOString()
    }
}

function calcPositionStats(totalMatchCount, matches, playerStats, cb) {
    var matchI = 1;
    matches.forEach(function (match) {
        playerStats.totalMatchCount++;

        // identify player's team and initial position
        var playerTeamId = null, playerPosition = null;
        match.teams.forEach(function (team) {
            //console.log(team);
            if (!playerTeamId) {
                team.teammates.forEach(function (teammate) {
                    if (teammate.playerId.toHexString() == playerStats.playerId) {
                        playerTeamId = team.id;
                        playerPosition = teammate.position;
                    }
                })
            }
        });

        if (match.teamSize == 2) {
            playerStats.double.past.defence = JSON.parse(JSON.stringify(playerStats.double.defence));
            playerStats.double.past.attack = JSON.parse(JSON.stringify(playerStats.double.attack));
        }

        var matchScore = {"t1": 0, "t2": 0};
        match.games.forEach(function (game) {
            // only count completed games
            if (game.t1 != 0 || game.t2 != 0) {
                if (match.teamSize == 1) {
                    playerStats.single.totalGameCount++;
                } else {
                    playerStats.double.totalGameCount++;
                }

                // count general score
                if (game.t1 > game.t2) {
                    matchScore.t1++;
                } else {
                    matchScore.t2++;
                }

                // count conceded goals as goalie
                // and scored whilst in attack
                if (match.teamSize == 2) {
                    //console.log(playerStats.playerId);
                    if (playerPosition == POSITION.DEFENCE) {
                        playerStats.double.defence.games++;
                        playerStats.double.defence.goalsConceded += game[opponentTeamId(playerTeamId)];
                    }
                    if (playerPosition == POSITION.ATTACK) {
                        playerStats.double.attack.games++;
                        playerStats.double.attack.goalsScored += game[playerTeamId];
                    }
                    playerPosition = switchPosition(playerPosition);
                }
            }
        });

        if (match.teamSize == 1) {
            playerStats.single.totalWinGameCount += matchScore[playerTeamId];
        } else {
            playerStats.double.totalWinGameCount += matchScore[playerTeamId];
//                        	if (matchI != totalMatchCount) {
//	                            playerStats.double.past.defence = JSON.parse(JSON.stringify(playerStats.double.defence));
//	                            playerStats.double.past.attack = JSON.parse(JSON.stringify(playerStats.double.attack));
            if (playerStats.playerId == '555a438107b15c0365761d0f') {
//					console.log("past:" + JSON.stringify(playerStats.double.defence));
//					console.log("before last match" + JSON.stringify(match));
            }

            playerStats.double.past.defence.goalsConcededPerGame = +(playerStats.double.past.defence.goalsConceded / playerStats.double.past.defence.games).toFixed(2);
            playerStats.double.past.attack.goalsScoredPerGame = +(playerStats.double.past.attack.goalsScored / playerStats.double.past.attack.games).toFixed(2);
//	                        } else if (matchI == totalMatchCount) {
//if (playerStats.playerId == '555a438107b15c0365761d0f') {
//					console.log("T:" + JSON.stringify(playerStats.double.defence));
//					console.log("last match" + JSON.stringify(match));
//}
//				}
        }

        matchI++;
    }, function () {
        playerStats.double.defence.goalsConcededPerGame = +(playerStats.double.defence.goalsConceded / playerStats.double.defence.games).toFixed(2);
        playerStats.double.attack.goalsScoredPerGame = +(playerStats.double.attack.goalsScored / playerStats.double.attack.games).toFixed(2);

        playerStats.double.defence.progress = {
            value: +(playerStats.double.defence.goalsConcededPerGame -
                playerStats.double.past.defence.goalsConcededPerGame).toFixed(2)
        };
        playerStats.double.defence.progress.good = playerStats.double.defence.progress.value < 0;

        playerStats.double.attack.progress = {
            value: +(playerStats.double.attack.goalsScoredPerGame -
                playerStats.double.past.attack.goalsScoredPerGame).toFixed(2)
        };
        playerStats.double.attack.progress.good = playerStats.double.attack.progress.value > 0;

        if (playerStats.double.attack.games + playerStats.double.defence.games > 10) {
            playerStats.double.simpleScore = +(1 / (playerStats.double.defence.goalsConcededPerGame) *
                (playerStats.double.attack.goalsScoredPerGame)).toFixed(2);
        } else {
            playerStats.double.simpleScore = null;
        }

        cb();
    });
}

app.get("/stats/teamStats", function (req, res) {
    MongoClient.connect(url, function (err, db) {
        var sideStats = {single: {}, double: {}};
        var stats = {};
        var cursor = db.collection('matches').find();
//		.sort({'date': -1})
//                .limit(100);
        cursor.forEach(function (match) {
            if (+match.teamSize == 2) {
                var teamMap = match.teams.reduce(function (teamMap, team) {
                    teamMap[team.id] = composeTeamId(team.teammates);
                    return teamMap;
                }, {});

                // init stats for a team
                COMMAND_IDS.forEach(function (id) {
                    if (!stats[teamMap[id]]) {
                        stats[teamMap[id]] = {
                            gamesTotal: 0,
                            gamesWon: 0,
                            winPercent: 0
                        };
                    }
                });

                match.games.forEach(function (game) {
                    COMMAND_IDS.forEach(function (id) {
                        if (game['t1'] != 0 || game['t2'] != 0) {
                            stats[teamMap[id]].gamesTotal++;
                            if (game[id] == 5) {
                                stats[teamMap[id]].gamesWon++;

                                // check side-stats
                                updateSideStats(sideStats.double, id);
                            }
                            stats[teamMap[id]].winPercent = +(stats[teamMap[id]].gamesWon / stats[teamMap[id]].gamesTotal * 100).toFixed(0);
                        }
                    });
                });
            } else {
                match.games.forEach(function (game) {
                    if (game['t1'] == 5) {
                        updateSideStats(sideStats.single, 't1');
                    } else if (game['t2'] == 5) {
                        updateSideStats(sideStats.single, 't2');
                    }
                });
            }
        }, function () {
            db.close();

            var teamResult = [];
            for (var team in stats) {
                var teamStats = {
                    teammates: team.split('_'),
                    results: stats[team]
                };
                if (stats[team].gamesTotal < 10) {
                    teamStats.results.winPercent = stats[team].gamesTotal / 100;
                    teamStats.disabled = true;
                }

                teamResult.push(teamStats);
            }
            teamResult.sort(function (t1, t2) {
                return t2.results.winPercent - t1.results.winPercent;
            });

            var result = {
                teamStats: teamResult,
                sideStats: sideStats
            };

            res.setHeader("Content-Type", "application/json;charset=utf-8");
            res.end(JSON.stringify(result));
        });
    });

    function composeTeamId(teammates) {
        return teammates.map(function (t) {
            return t.playerId;
        }).sort().join('_');
    }
});

app.get('/stats/player/:id', function (req, res) {
    var playerId = req.params.id;

    MongoClient.connect(url, function (err, db) {
        var stats = {
            totalMatches: 0
        };
        var cursor = db.collection('matches').find({'teams.teammates.playerId': ObjectId(playerId)});
        cursor.forEach(function () {
            stats.totalMatches++;
        }, function () {
            db.close();

            res.setHeader("Content-Type", "application/json;charset=utf-8");
            res.end(JSON.stringify(stats));
        });
    });
});

var POSITION = {
    'ATTACK': 'атака',
    'DEFENCE': 'защита'
};

var COMMAND_IDS = ['t1', 't2'];

function switchPosition(position) {
    if (position == POSITION.ATTACK) {
        return POSITION.DEFENCE;
    } else {
        return POSITION.ATTACK;
    }
}

function opponentTeamId(teamId) {
    if (teamId == "t1") {
        return "t2";
    } else {
        return "t1";
    }
}

function updateSideStats(sideStats, teamId) {
    var name = {'t1': 'Окно', 't2': 'Стол'}[teamId];
    if (!sideStats[name]) {
        sideStats[name] = 0;
    }
    sideStats[name]++;
}

app.listen(8787);

console.log("Server started");


module.exports = app;
