# SMISS TABLE SOCCER

## ENDPOINTS

### GET `/player`
    Returns all available players in json

#### Response example

```json
[{"_id":"555a436d07b15c0365761d0c","name":"Дима"},{"_id":"565ec820f94029d7392cfe83","disabled":false,"name":"Женя Кравченко"},{"_id":"55e4381d783dc2e60b2b7728","disabled":false,"name":"Женя Коренцов"},{"_id":"555a437b07b15c0365761d0e","name":"Паша","disabled":true},{"_id":"555a437607b15c0365761d0d","name":"Максим","disabled":true},{"_id":"555a438107b15c0365761d0f","name":"Артем","disabled":false},{"_id":"56c72e41a5bb0a1b00d56951","name":"Фёдор"},{"_id":"56f3bc0b6e5e58b1945328f7","name":"Андрей"},{"_id":"571639a6ef163ae26234ed87","name":"Миша"},{"_id":"56d0483ebfe9bdc5368de482","name":"Валера","disabled":true},{"_id":"587e4525ca567814e6a1f53c","name":"Ксюша"},{"_id":"5683cf18b2a360dba6a71169","disabled":true,"name":"Аня"},{"_id":"58dd2cb1ce460ccf6eeae656","name":"Диана"},{"_id":"566ac92425d91f0559d0c4d5","name":"Вадим","disabled":true},{"_id":"5922c03e11db1ef87f003e7c","name":"Игорь","disabled":true},{"_id":"598069c29ebcffa49cc6b455","name":"Дима Пусный"},{"_id":"59cbc6e5fcf17d362a4b6ed9","name":"Артем Лашин"},{"_id":"59ef48226aa4443c03eef002","name":"Маша"}]
```

### GET `/match`
    Returns mathes

#### Query params

`count` - __Number__ - target amount of matches. If -1 - returns all matches

#### Response example

```json
[{"_id":"5bb60fbda160f35d300885fb","date":"2018-10-04T13:02:08.371Z","teamSize":2,"teams":[{"id":"t1","name":"Окно","teammates":[{"position":"атака","playerId":"555a438107b15c0365761d0f"},{"position":"защита","playerId":"56c72e41a5bb0a1b00d56951"}]},{"id":"t2","name":"Стол","teammates":[{"position":"атака","playerId":"56f3bc0b6e5e58b1945328f7"},{"position":"защита","playerId":"59cbc6e5fcf17d362a4b6ed9"}]}],"games":[{"t1":2,"t2":5},{"t1":5,"t2":2},{"t1":5,"t2":2},{"t1":5,"t2":1},{"t1":0,"t2":0}]},{"_id":"5bb4c0a2a160f35d300885fa","date":"2018-10-03T13:13:40.197Z","teamSize":2,"teams":[{"id":"t1","name":"Окно","teammates":[{"position":"атака","playerId":"59cbc6e5fcf17d362a4b6ed9"},{"position":"защита","playerId":"56f3bc0b6e5e58b1945328f7"}]},{"id":"t2","name":"Стол","teammates":[{"position":"атака","playerId":"555a438107b15c0365761d0f"},{"position":"защита","playerId":"56c72e41a5bb0a1b00d56951"}]}],"games":[{"t1":4,"t2":5},{"t1":2,"t2":5},{"t1":1,"t2":5},{"t1":0,"t2":0},{"t1":0,"t2":0}]},{"_id":"5bb2558da160f35d300885f9","date":"2018-10-01T17:12:26.762Z","teamSize":1,"teams":[{"id":"t1","name":"Окно","teammates":[{"position":"","playerId":"555a438107b15c0365761d0f"}]},{"id":"t2","name":"Стол","teammates":[{"position":"","playerId":"56c72e41a5bb0a1b00d56951"}]}],"games":[{"t1":5,"t2":4},{"t1":1,"t2":5},{"t1":3,"t2":5}]}]
```

### GET `/playerStats`
    Returns player statistics

#### Query params

`season` - __BOOL__ - If true - returns stats for current month

#### Response example

```json
[{"playerId":"56f3bc0b6e5e58b1945328f7","totalMatchCount":201,"single":{"totalGameCount":14,"totalWinGameCount":5},"double":{"totalGameCount":764,"totalWinGameCount":343,"defence":{"games":381,"goalsConceded":1437,"goalsConcededPerGame":3.77,"progress":{"value":0,"good":false}},"attack":{"games":383,"goalsScored":1360,"goalsScoredPerGame":3.55,"progress":{"value":0,"good":false}},"simpleScore":0.94,"past":{"defence":{"games":379,"goalsConceded":1427,"goalsConcededPerGame":3.77},"attack":{"games":381,"goalsScored":1353,"goalsScoredPerGame":3.55}}}},{"playerId":"5683cf18b2a360dba6a71169","totalMatchCount":66,"single":{"totalGameCount":0,"totalWinGameCount":0},"double":{"totalGameCount":275,"totalWinGameCount":135,"defence":{"games":144,"goalsConceded":494,"goalsConcededPerGame":3.43,"progress":{"value":0.01,"good":false}},"attack":{"games":131,"goalsScored":440,"goalsScoredPerGame":3.36,"progress":{"value":0,"good":false}},"simpleScore":0.98,"past":{"defence":{"games":142,"goalsConceded":485,"goalsConcededPerGame":3.42},"attack":{"games":129,"goalsScored":434,"goalsScoredPerGame":3.36}}}},{"playerId":"555a438107b15c0365761d0f","totalMatchCount":326,"single":{"totalGameCount":209,"totalWinGameCount":127},"double":{"totalGameCount":987,"totalWinGameCount":595,"defence":{"games":476,"goalsConceded":1673,"goalsConcededPerGame":3.51,"progress":{"value":-0.01,"good":true}},"attack":{"games":511,"goalsScored":2141,"goalsScoredPerGame":4.19,"progress":{"value":0,"good":false}},"simpleScore":1.19,"past":{"defence":{"games":474,"goalsConceded":1670,"goalsConcededPerGame":3.52},"attack":{"games":509,"goalsScored":2134,"goalsScoredPerGame":4.19}}}}]
```


### GET `/teamStats`
    Returns statistics for teams and sides

#### Response example

```json
{"teamStats":[{"teammates":["555a438107b15c0365761d0f","56c72e41a5bb0a1b00d56951"],"results":{"gamesTotal":134,"gamesWon":95,"winPercent":71}},{"teammates":["555a436d07b15c0365761d0c","56c72e41a5bb0a1b00d56951"],"results":{"gamesTotal":112,"gamesWon":74,"winPercent":66}},{"teammates":["555a436d07b15c0365761d0c","555a438107b15c0365761d0f"],"results":{"gamesTotal":344,"gamesWon":227,"winPercent":66}},{"teammates":["555a437607b15c0365761d0d","555a438107b15c0365761d0f"],"results":{"gamesTotal":99,"gamesWon":63,"winPercent":64}},{"teammates":["555a436d07b15c0365761d0c","56f3bc0b6e5e58b1945328f7"],"results":{"gamesTotal":184,"gamesWon":112,"winPercent":61}},{"teammates":["565ec820f94029d7392cfe83","56f3bc0b6e5e58b1945328f7"],"results":{"gamesTotal":25,"gamesWon":15,"winPercent":60}},{"teammates":["555a437607b15c0365761d0d","555a437b07b15c0365761d0e"],"results":{"gamesTotal":43,"gamesWon":25,"winPercent":58}},{"teammates":["555a436d07b15c0365761d0c","5683cf18b2a360dba6a71169"],"results":{"gamesTotal":105,"gamesWon":61,"winPercent":58}},{"teammates":["555a438107b15c0365761d0f","5683cf18b2a360dba6a71169"],"results":{"gamesTotal":43,"gamesWon":25,"winPercent":58}},{"teammates":["555a436d07b15c0365761d0c","56d0483ebfe9bdc5368de482"],"results":{"gamesTotal":117,"gamesWon":67,"winPercent":57}},{"teammates":["555a438107b15c0365761d0f","566ac92425d91f0559d0c4d5"],"results":{"gamesTotal":80,"gamesWon":44,"winPercent":55}},{"teammates":["555a436d07b15c0365761d0c","555a437607b15c0365761d0d"],"results":{"gamesTotal":71,"gamesWon":39,"winPercent":55}}], "sideStats":{"single":{"Стол":140,"Окно":130},"double":{"Окно":703,"Стол":763}}}
```


### PUT `/match`
    Saves new match

#### Payload example

For match with one player

```json
{"date":"2018-10-05T10:42:44.197Z","teamSize":1,"teams":[{"id":"t1","name":"Окно","teammates":[{"position":"","playerId":"56c72e41a5bb0a1b00d56951"}]},{"id":"t2","name":"Стол","teammates":[{"position":"","playerId":"56c72e41a5bb0a1b00d56951"}]}],"games":[{"t1":0,"t2":0},{"t1":0,"t2":0},{"t1":0,"t2":0}]}
```

For match with two players

```json
{"date":"2018-10-05T10:43:38.800Z","teamSize":2,"teams":[{"id":"t1","name":"Окно","teammates":[{"position":"атака","playerId":"56c72e41a5bb0a1b00d56951"},{"position":"защита","playerId":"56c72e41a5bb0a1b00d56951"}]},{"id":"t2","name":"Стол","teammates":[{"position":"атака","playerId":"56c72e41a5bb0a1b00d56951"},{"position":"защита","playerId":"56c72e41a5bb0a1b00d56951"}]}],"games":[{"t1":0,"t2":0},{"t1":0,"t2":0},{"t1":0,"t2":0},{"t1":0,"t2":0},{"t1":0,"t2":0}]}
```

### DELETE `/match/:id`
    Deletes match with id from history